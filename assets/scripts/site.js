//second-nav
$(".second-nav-contents li").click(function() {
  let navName = $(this).attr("name");
  $(".secondary-nav-content").addClass("d-none");
  $(`.${navName}`).removeClass("d-none");
  if (!$(this).hasClass("active")) {
    $(this)
      .siblings()
      .removeClass("active");
    $(this).addClass("active");
  }
});

// Get the modal
var modal = document.getElementById("campaign-Modal");

// Get the <span> element that closes the modal
var span = document.getElementsByClassName("close")[0];

// When the user clicks on the button, open the modal
$(".modal-btn").click(function() {
  modal.style.display = "block";
});

// When the user clicks on <span> (x), close the modal
span.onclick = function() {
  modal.style.display = "none";
};

// When the user clicks anywhere outside of the modal, close it
window.onclick = function(event) {
  if (event.target == modal) {
    modal.style.display = "none";
  }
};

//
// table script
//
$(document).ready(function() {
  $("#reportsChart").DataTable({
    ajax: "./assets/scripts/data.txt",
    last: true,
    pagingType: "first_last_numbers",
    language: {
      paginate: {
        first: "اولی",
        last: "آخری",
        next: false,
        previous: false
      }
    },
    bLengthChange: false, //thought this line could hide the LengthMenu
    bInfo: false
  });
});

/* Custom filtering function which will search data in column four between two values */
$.fn.dataTable.ext.search.push(function(settings, data, dataIndex) {
  var minVisit = parseInt($("#min-visit").val(), 10);
  var maxVisit = parseInt($("#max-visit").val(), 10);
  var visit = parseFloat(data[2].replace(/,/g, "")) || 0;

  if (
    (isNaN(minVisit) && isNaN(maxVisit)) ||
    (isNaN(minVisit) && visit <= maxVisit) ||
    (minVisit <= visit && isNaN(maxVisit)) ||
    (minVisit <= visit && visit <= maxVisit)
  ) {
    return true;
  }
  return false;
});

$.fn.dataTable.ext.search.push(function(settings, data, dataIndex) {
  var minSent = parseInt($("#min-sent").val(), 10);
  var maxSent = parseInt($("#max-sent").val(), 10);
  var sent = parseFloat(data[1].replace(/,/g, "")) || 0;

  if (
    (isNaN(minSent) && isNaN(maxSent)) ||
    (isNaN(minSent) && sent <= maxSent) ||
    (minSent <= sent && isNaN(maxSent)) ||
    (minSent <= sent && sent <= maxSent)
  ) {
    return true;
  }
  return false;
});

$.fn.dataTable.ext.search.push(function(settings, data, dataIndex) {
  var minClick = parseInt($("#min-click").val(), 10);
  var maxClick = parseInt($("#max-click").val(), 10);
  var click = parseFloat(data[3].replace(/,/g, "")) || 0;

  if (
    (isNaN(minClick) && isNaN(maxClick)) ||
    (isNaN(minClick) && click <= maxClick) ||
    (minClick <= click && isNaN(maxClick)) ||
    (minClick <= click && click <= maxClick)
  ) {
    return true;
  }
  return false;
});

$(document).ready(function() {
  var table = $("#reportsChart").DataTable();

  // Event listener to the two range filtering inputs to redraw on input
  $("#min-visit, #max-visit").keyup(function() {
    table.draw();
  });

  // Event listener to the two range filtering inputs to redraw on input
  $("#min-sent, #max-sent").keyup(function() {
    table.draw();
  });

  // Event listener to the two range filtering inputs to redraw on input
  $("#min-click, #max-click").keyup(function() {
    table.draw();
  });
});
$(".filter-btn").click(function() {
  $(".table-filter-wrapper")
    .fadeToggle()
    .css("display", "flex");
  $("#reportsChart_wrapper .dataTables_filter").fadeToggle();
});

//second table

$(document).ready(function() {
  $("#reportsChart2").DataTable({
    ajax: "./assets/scripts/data2.txt",
    last: true,
    pagingType: "first_last_numbers",
    language: {
      paginate: {
        first: "اولی",
        last: "آخری",
        next: false,
        previous: false
      }
    },
    bLengthChange: false, //thought this line could hide the LengthMenu
    bInfo: false
  });
});

// report-btn fun

$("body").on("click", ".campaign-details", function() {
  $(".report-contents").fadeOut();
  $(".single-campaign").fadeIn();
});

$("body").on("click", "#single-campaign-back", function() {
  $(".report-contents").fadeIn();
  $(".single-campaign").fadeOut();
});

//new advertisement btn
var activenav = "";
$(".add-new-advertise").click(function() {
  activenav = $(".second-nav-contents li.active").attr("name");
  $("." + activenav).fadeOut();
  $(".second-nav").fadeOut();
  $(".new-ad").fadeIn();
});
$("body").on("click", "#new-add-back", function() {
  $("." + activenav).fadeIn();
  $(".second-nav").fadeIn();
  $(".new-ad").fadeOut();
});
